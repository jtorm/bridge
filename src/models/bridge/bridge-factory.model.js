/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { bridgeModel } = require('./../bridge.model'),

  bridgeFactoryModel = config => {
    const bridgeInstance = { ...bridgeModel };

    bridgeInstance.config = {
      ...bridgeInstance.config,
      ...config
    };

    return bridgeInstance;
  }
;

module.exports = {
  bridgeFactoryModel
};
