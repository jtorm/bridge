/*! (c) jTorm and other contributors | https://jtorm.com/license */

const bridgeModel = {
  config: {
    "domain": "",
    "domain_map": "",
    "locale": "en-US"
  }
};

module.exports = {
  bridgeModel
};
