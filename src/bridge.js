/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { bridgeModel } = require('./models/bridge.model'),
  { bridgeFactoryModel } = require('./models/bridge/bridge-factory.model')
;

module.exports = {
  bridgeModel,
  bridgeFactoryModel
};
